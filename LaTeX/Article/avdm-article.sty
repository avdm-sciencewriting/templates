\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{avdm-article}[2020/12/04 Custom Package]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Page geometry setup                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The geometry package expands the text on the page to occupy a little more space
\usepackage{geometry}
\geometry{
    a4paper,
    total={170mm,257mm},
    left=20mm,
    right=20mm,
    top=15mm,
    bottom=15mm,
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tools for rotating content and pages                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If you only want to rotate a figure or table, without affecting where the
% page decorations (line numbers, page number, etc) are printed, wrap the
% relevant section in \begin{landcape} ... \end{landscape}
%\usepackage{lscape}
% If you want to rotate the entire page, including its decorations, wrap the
% relevant section in \begin{landscape} ... \end{landscape}. This starts a new
% page in landscape orientation. I'm using this as default because it is easier
% to view the PDF. With the lscape package, you have to manually rotate the
% relevant page in your PDF viewer.
\usepackage{pdflscape}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Text encoding                                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define the text encodings
\usepackage[LGR,T1]{fontenc}
% Set input encoding so that UTF8 characters are handled more easily
\usepackage[utf8]{inputenc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author block formatting                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Be able to typeset the authors block and their affiliations
\usepackage{authblk}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tables   formatting                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Packages for tables, provides tabu and longtabu
% https://ctan.org/pkg/tabu
\usepackage{longtable,array,xcolor,colortbl,tabu}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Headings formatting                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A package providing an interface to sectioning commands for selection from 
% various title styles. E.g., marginal titles and to change the font of all 
% headings with a single command, also providing simple one-step page styles. 
% Also includes a package to change the page styles when there are floats in a 
% page. You may assign headers/footers to individual floats, too.
% https://ctan.org/pkg/titlesec
\usepackage{titlesec}
\titleformat{\section}
    {\normalfont\large\sffamily}
    {\thesection}
    {1em}
    {}
\titleformat{\subsection}
    {\normalfont\normalsize\sffamily}
    {\thesubsection}
    {1em}
    {}
\titleformat{\subsubsection}
    {\normalfont\small\sffamily}
    {\thesubsubsection}
    {1em}
    {}
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract formatting                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The abstract package gives you control over the typesetting of the abstract 
% environment, and in particular provides for a one column abstract in a two 
% column paper.
% https://ctan.org/pkg/abstract
\usepackage{abstract}
\renewcommand\abstractnamefont{\bfseries\sffamily}
\renewcommand\abstracttextfont{\normalfont\small\sffamily}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Citations and bibliography                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[
  backend=biber,            % Use biber to sort the bibliography
  bibencoding=utf8,         % The text encoding in your .bib file
  style=authoryear-icomp,
  %style=authoryear,        % Use with \parencite, yields (Einstein, 1905)
  %style=numeric,           % [1,2,3]
  %style=numeric-comp,      % Compressed numeric, [1-3]
  %style=alphabetic,        % [Eins05]
  maxcitenames=3,           % Maximum number of names displayed in citations
  maxbibnames=1000,         % Maximum number of names displayed in bibliography
  sortlocale=en_ZA,
  natbib=true,
  url=false,
  doi=false,                % Include the paper's DOI in the bibliography
  eprint=false
]{biblatex}
% Configuration for the bibliography
\DeclareFieldFormat[book,inbook,article]{title}{#1} % Remove quotations
\DeclareFieldFormat[article]{pages}{#1} % Remove ``pp.'' from articles page ranges
\renewbibmacro{in:}{% Remove In: from article titles
  \ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}}
\addbibresource{./bibliography.bib}
\renewcommand*{\bibfont}{\footnotesize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hyperlinks                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[
    colorlinks,
    breaklinks,
    bookmarks=false,
    pdfstartview=Fit,  % for fitting entire page; FitW just fits width
    pdfview=Fit,       % after traversing a hyperlink
    linkcolor=grey,
    urlcolor=grey,
    citecolor=black,
    hyperfootnotes=false
]{hyperref}
\usepackage[figure,table]{hypcap} % Correct a problem with hyperref
\urlstyle{rm} %so it doesn't use a typewriter font for url's.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paragraph formatting                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Configuration for how paragraphs should be formatted
\setlength{\parindent}{0em} % Remove paragraph indentations
\setlength{\parskip}{.5em} % Space between paragraphs
\usepackage{setspace} % Provides \doublespacing \onehalfspacing \singlespacing
\hyphenpenalty=6000
% Use Times as serif font, Helvetica as sans-serif, and Courier as typewriter.
% https://ctan.org/pkg/mathptmx?lang=en
\usepackage{mathptmx,helvet,courier}
% Enable this if you want to use the sans-serif font as the default font
%\renewcommand{\familydefault}{\sfdefault}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Utilities and general formatting                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This package is used to import the parts of the document
\usepackage{import}
% Align captions for tables and figures to text width
\usepackage[singlelinecheck=false]{caption}
% The blindtext package is not needed when you produce your document. It is just
% here to provide some dummy text.
% https://www.ctan.org/pkg/blindtext
\usepackage{blindtext}
% Enable the use of SI unit symbols, such as \degreeCelsius
% https://ctan.org/pkg/siunitx?lang=en
\usepackage{siunitx}
% By default SI units are typeset in the default upright font. This detects
% the current font and typesets the units in the same font.
\sisetup{detect-all}
% Enable the use of symbols such as \texteuro for the Euro symbol
\usepackage{textcomp}
% If you want line numbers, enable this package
% Also put the \linenumbers command in the document where you want line
% numbers to start printing.
\usepackage{lineno}
 
