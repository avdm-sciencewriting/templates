---
title: A MarkDown article template
csl: "genetics.csl"
link-citations: true
figPrefix:
    - "Fig."
    - "Figs"
linkReferences: true
nameInLink: true
---

## Frontmatter, not part of the article

- Recommended software for writing MarkDown is ReText.
- The pandoc documentation is at [Pandoc Manual](http://pandoc.org/MANUAL.html)
- Prepare your .bib file in BibLaTeX with KBibtex, Jabref, or whatever you need
- Export the path to your .bib file as a $BIB environment variable, i.e. in your ~/.bashrc: `export BIB=/path/to/your/references.bib` and then source your ~/.bashrc file: `source ~/.bashrc`
- Make sure that you have `pandoc` and `pandoc-citeproc` installed. The newest version of `pandoc` already includes `pandoc-crossref`.
- In order to convert this file to a PDF, install `texlive`.
- You can find CSL files for a massive number of journals [here](https://github.com/citation-style-language/styles). For this template, we are using the `genetics.csl` style which is in the same directory as the current file. There is also  a [visual citation style editor](https://editor.citationstyles.org/visualEditor/) to manually configure your style.
- On linux you can use the `autocompile.sh` script to automatically compile a PDF as soon as the source `.md` file changes. You will need to install `entr` on your Linux system.

This is **bold**

This is *italics* and this is also _italics_

---

## Abstract

Vestibulum nec lobortis mauris. Suspendisse vitae mattis sem.

---

## Introduction

Donec consequat semper bibendum. Vestibulum nec lobortis mauris. Suspendisse vitae mattis sem. Here is a citation [@Fonseca2020a].

## Materials and Methods

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur ullamcorper finibus. Ut in maximus massa. Proin erat mauris, dignissim quis magna quis, interdum ultricies velit ([@fig:kitten]). Nullam a nunc suscipit, lobortis mi tristique, euismod felis [@Haridas2020a; @Lanciano2020a]. You can have footnotes as well [^1].

[^1]: It is recommended that you put the footnote text immediately after the paragraph where the footnote reference is. All footnotes will be displayed at the end of the document, after the references.

## Results

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur ullamcorper finibus. Ut in maximus massa. Proin erat mauris, dignissim quis magna quis, interdum ultricies velit. Nullam a nunc suscipit, lobortis mi tristique, euismod felis. In lectus diam, sollicitudin id suscipit sit amet, tincidunt eget sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer blandit augue ex, nec pellentesque arcu interdum eget. Phasellus non ex felis. Duis varius orci eget ullamcorper porta. Mauris gravida, quam quis tincidunt ultrices, nulla ligula feugiat eros, congue luctus arcu dui sit amet velit. Maecenas sodales ligula fringilla nulla egestas fermentum. Donec consequat semper bibendum. Vestibulum nec lobortis mauris. Suspendisse vitae mattis sem.

# Discussion

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur ullamcorper finibus. Ut in maximus massa. Proin erat mauris, dignissim quis magna quis, interdum ultricies velit. Nullam a nunc suscipit, lobortis mi tristique, euismod felis. In lectus diam, sollicitudin id suscipit sit amet, tincidunt eget sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer blandit augue ex, nec pellentesque arcu interdum eget. Phasellus non ex felis. Duis varius orci eget ullamcorper porta. Mauris gravida, quam quis tincidunt ultrices, nulla ligula feugiat eros, congue luctus arcu dui sit amet velit. Maecenas sodales ligula fringilla nulla egestas fermentum. Donec consequat semper bibendum. Vestibulum nec lobortis mauris. Suspendisse vitae mattis sem.

# Conclusions

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur ullamcorper finibus. Ut in maximus massa. Proin erat mauris, dignissim quis magna quis, interdum ultricies velit. Nullam a nunc suscipit, lobortis mi tristique, euismod felis. In lectus diam, sollicitudin id suscipit sit amet, tincidunt eget sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer blandit augue ex, nec pellentesque arcu interdum eget. Phasellus non ex felis. Duis varius orci eget ullamcorper porta. Mauris gravida, quam quis tincidunt ultrices, nulla ligula feugiat eros, congue luctus arcu dui sit amet velit. Maecenas sodales ligula fringilla nulla egestas fermentum. Donec consequat semper bibendum. Vestibulum nec lobortis mauris. Suspendisse vitae mattis sem.

## Acknowledgements

Put your acknowledgements here.

## Tables

## Figures

![This is a kitten.](./images/kitten.jpg){#fig:kitten}

## References