#!/usr/bin/env sh
echo "+---------------------------------------------------------------------+"
echo "| COMPILING TO ALL AVAILABLE FORMATS IN THE to*.sh SCRIPTS            |"
echo "+---------------------------------------------------------------------+"

echo "Compiling to .docx"
./todocx.sh
echo "...done"
echo ""
echo "Compiling to .html"
./tohtml.sh
echo "...done"
echo ""
echo "Compiling to .pdf"
./topdf.sh
echo "...done"
